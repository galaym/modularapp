﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using ModularApp.Core;

namespace ModularApp.Consol
{
    class Program
    {
        private static string ModulePath
        {
            get { return Directory.GetCurrentDirectory() + @"\Modules"; }
        }

        private static List<ModuleMetaData> Modules { get; set; }
        private static List<ModuleBackgroundWorker> ModuleBackgroundWorkers { get; set; }

        static void Main(string[] args)
        {
            Modules = new List<ModuleMetaData>();
            ModuleBackgroundWorkers = new List<ModuleBackgroundWorker>();
            ReloadModules();
            do
            {
                Console.Write("Enter comand (? - list commands): ");
                var line = Console.ReadLine();
                switch (line)
                {
                    case "?":
                        ListCommands();
                        break;
                    case "exit":
                        Environment.Exit(0);
                        break;
                    case "list":
                        ListModules();
                        break;
                    case "reload":
                        ReloadModules();
                        break;
                    case "status":
                        StatusModules();
                        break;
                    case "run":
                        RunWork();
                        break;
                    case "stop":
                        StopWork();
                        break;
                }
                Thread.Sleep(100);
            } while (true);
        }

        private static void ListCommands()
        {
            Console.WriteLine();
            Console.WriteLine("? - list commands");
            Console.WriteLine("list - list modules");
            Console.WriteLine("reload - reload list modules");
            Console.WriteLine("run - run task");
            Console.WriteLine("stop - stop task");
            Console.WriteLine("status - status task");
            Console.WriteLine("exit - close programm");
            Console.WriteLine();
        }

        private static void ListModules()
        {
            Console.WriteLine("===========================================================================");
            Console.WriteLine("{0,4} {1,15} {2,10} {3,10} {4,20}", "Id", "Name", "Version", "Status", "Description");
            foreach (var module in Modules)
            {
                Console.WriteLine("{0,4} {1,15} {2,10} {3,10} {4,20}", module.Id, module.Name, module.Version, module.Status, module.Description);
            }
            Console.WriteLine("===========================================================================");
        }

        private static void ReloadModules()
        {
            var managementModules = new ManagementModules();
            var modules = managementModules.LoadModules(ModulePath).ToList();
            modules.Where(module => !Modules.Any(module.Equals)).ToList().ForEach(m =>
                {
                    m.Id = Modules.Any() ? Modules.Max(mo => mo.Id) + 1 : 1;
                    Modules.Add(m);
                });
            Modules.Where(m => !modules.Any(m.Equals) && m.Status != ModuleStatus.Running).ToList().ForEach(m =>
                {
                    Modules.Remove(m);
                    ModuleBackgroundWorkers.RemoveAll(w => w.Id == m.Id);
                });
            Console.WriteLine("Updating the list of modules executed");
        }

        private static void StatusModules()
        {
            Console.Write("Enter Id module: ");
            var idString = Console.ReadLine();
            int id;
            int.TryParse(idString, out id);
            var module = Modules.FirstOrDefault(m => m.Id == id);
            if (module == null)
            {
                Console.WriteLine("Module not found");
                return;
            }
            Console.WriteLine("Module status - {0}", module.Status);
            var work = ModuleBackgroundWorkers.FirstOrDefault(w => w.Id == module.Id);
            if (work != null && module.Status == ModuleStatus.Running)
                Console.WriteLine("Work complite - {0}", work.Status);
        }

        private static void RunWork()
        {
            Console.Write("Enter Id module: ");
            var idString = Console.ReadLine();
            int id;
            int.TryParse(idString, out id);
            var module = Modules.FirstOrDefault(m => m.Id == id);
            if (module == null)
            {
                Console.WriteLine("Module not found!");
                return;
            }
            if (module.Status == ModuleStatus.Running)
            {
                Console.WriteLine("The module was already started!");
                return;
            }
            var moduleBackgroundWorker = ModuleBackgroundWorkers.FirstOrDefault(w => w.Id == module.Id);
            var managementModules = new ManagementModules();
            var moduleInstance = managementModules.CreateInstance<IModule>(module);
            if (moduleBackgroundWorker == null) moduleBackgroundWorker = new ModuleBackgroundWorker(moduleInstance.Start, module.Id);
            ModuleBackgroundWorkers.Add(moduleBackgroundWorker);
            moduleBackgroundWorker.Start();
            module.Status = ModuleStatus.Running;
            Console.WriteLine("Module is Run");
        }

        private static void StopWork()
        {
            Console.Write("Enter Id module: ");
            var idString = Console.ReadLine();
            int id;
            int.TryParse(idString, out id);
            var module = Modules.FirstOrDefault(m => m.Id == id);
            if (module == null)
            {
                Console.WriteLine("Module not found!");
                return;
            }
            var moduleBackgroundWorker = ModuleBackgroundWorkers.FirstOrDefault(w => w.Id == module.Id);

            if (module.Status != ModuleStatus.Running || moduleBackgroundWorker == null)
            {
                Console.WriteLine("The module wasn't started!");
                return;
            }
            moduleBackgroundWorker.Cencel();
            module.Status = ModuleStatus.Done;
            Console.WriteLine("The module is stopped.");
        }
    }
}
