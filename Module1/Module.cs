﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ModularApp.Core;

namespace Module1
{
    public class Module : IModule
    {
        private BackgroundWorker BackgroundWorker { get; set; }
        private object Obj { get; set; }

        public void Start(BackgroundWorker backgroundWorker, object obj = null)
        {
            BackgroundWorker = backgroundWorker;
            BackgroundWorker.WorkerSupportsCancellation = true;
            Obj = obj;
            Console.WriteLine("Module1 is running!");
            for (int i = 0; i < 100; i++)
            {
                if ((BackgroundWorker.CancellationPending))
                {
                    Console.WriteLine("Module1 is stoped!");
                    break;
                }
                else
                {
                    Thread.Sleep(1000);
                    BackgroundWorker.ReportProgress(i, Obj);
                    //Console.WriteLine("Module 1 - " + i);
                }
            }
            Console.WriteLine("Module1 is Complite!");
        }
    }
}
