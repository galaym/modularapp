﻿namespace ModularApp.Core
{
    public enum ModuleStatus
    {
        Running,
        NotRunning,
        Error,
        Done
    }
}