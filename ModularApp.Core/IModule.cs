﻿using System;
using System.ComponentModel;

namespace ModularApp.Core
{
    public interface IModule
    {
        void Start(BackgroundWorker backgroundWorker, object obj = null);
    }
}
