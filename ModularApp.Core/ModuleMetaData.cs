﻿namespace ModularApp.Core
{
    public class ModuleMetaData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string Version { get; set; }
        public ModuleStatus Status { get; set; }

        protected bool Equals(ModuleMetaData other)
        {
            return string.Equals(Name, other.Name) && string.Equals(Path, other.Path) && string.Equals(Version, other.Version);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Path != null ? Path.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Version != null ? Version.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ModuleMetaData) obj);
        }
    }
}