﻿using System;

namespace ModularApp.Core
{
    public class ModuleWorkerEventArgs : EventArgs
    {
        public ModuleStatus ModuleStatus { get; set; }
        public object Result { get; set; }
    }
}