﻿using System;

namespace ModularApp.Core
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class PluginModuleAttribute : Attribute
    {
        public string Name;
        public string Descriptions;
        public string Version;

        public PluginModuleAttribute(string name, string descriptions, string version)
        {
            Name = name;
            Descriptions = descriptions;
            Version = version;
        }
    }
}