﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ModularApp.Core
{
    public class ManagementModules
    {
        public IEnumerable<ModuleMetaData> LoadModules(string directory)
        {
            var result = new List<ModuleMetaData>();
            try
            {
                var ddls = Directory.GetFiles(directory).Where(x => Path.GetExtension(x) == ".dll").ToList();
                foreach (var ddl in ddls)
                {
                    try
                    {
                        var assembly = Assembly.LoadFrom(ddl);
                        if (assembly == null) continue;
                        var attributes = assembly.GetCustomAttributes(typeof(PluginModuleAttribute), false);
                        if (!attributes.Any()) continue;
                        var pluginModuleAttribute = (PluginModuleAttribute)attributes.First();
                        result.Add(new ModuleMetaData
                            {
                                Name = pluginModuleAttribute.Name,
                                Description = pluginModuleAttribute.Descriptions,
                                Version = pluginModuleAttribute.Version,
                                Status = ModuleStatus.NotRunning,
                                Path = ddl
                            });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }


        public T CreateInstance<T>(ModuleMetaData moduleMetaData)
        {
            Object obj = null;
            Assembly assembly = null;
            try
            {
                var asname = AssemblyName.GetAssemblyName(moduleMetaData.Path);
                assembly = AppDomain.CurrentDomain.Load(asname);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            try
            {
                var moduleName = moduleMetaData.Name + '.' + typeof(T).Name.TrimStart('I');
                if (assembly != null) obj = assembly.CreateInstance(moduleName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return (T)obj;
        }
    }
}