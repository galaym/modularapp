﻿using System;
using System.ComponentModel;

namespace ModularApp.Core
{
    public class ModuleBackgroundWorker
    {
        public int Id { get; set; }
        public string Status { get; set; }

        public event EventHandler Event;
        public BackgroundWorker BackgroundWorker { get; set; }

        protected Action<BackgroundWorker, object> Action { get; set; }

        protected virtual void OnEvent(ModuleStatus status, object result)
        {
            var handler = Event;
            if (handler != null) handler(this, new ModuleWorkerEventArgs() { ModuleStatus = status, Result = result });
        }

        public ModuleBackgroundWorker(Action<BackgroundWorker, object> action, int id)
        {
            Id = id;
            this.Action = action;
            BackgroundWorker = new BackgroundWorker
                {
                    WorkerReportsProgress = true
                };
            BackgroundWorker.DoWork += DoWork;
            BackgroundWorker.RunWorkerCompleted += RunWorkerCompleted;
            BackgroundWorker.ProgressChanged += ProgressChanged;
        }

        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (BackgroundWorker.WorkerSupportsCancellation)
            {
                Status = e.ProgressPercentage.ToString();
            }
        }

        private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var isError = e.Error != null;
            object obj = null;
            string errorMessage = string.Empty;
            try
            {
                obj = e.Result;
            }
            catch (Exception ex)
            {
                isError = true;
                errorMessage = ex.Message;
            }
            OnEvent(isError ? ModuleStatus.Error : ModuleStatus.Done, obj);
            Status = isError ? errorMessage : "Complite";
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            Action(BackgroundWorker, e.Argument);
            if (BackgroundWorker.CancellationPending) e.Cancel = true;
                
        }

        public bool Start(object obj = null)
        {
            try
            {
                if (BackgroundWorker.IsBusy != true)
                {
                    BackgroundWorker.RunWorkerAsync();
                }
                else return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void Cencel()
        {
            if (BackgroundWorker.WorkerSupportsCancellation)
            {
                BackgroundWorker.CancelAsync();
            }
        }
    }
}