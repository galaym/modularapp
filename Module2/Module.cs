﻿using System;
using System.ComponentModel;
using System.Threading;
using ModularApp.Core;

namespace Module2
{
    public class Module : IModule
    {
        private BackgroundWorker BackgroundWorker { get; set; }
        private object Obj { get; set; }

        public void Start(BackgroundWorker backgroundWorker, object obj = null)
        {
            BackgroundWorker = backgroundWorker;
            BackgroundWorker.WorkerSupportsCancellation = true;
            Obj = obj;
            Console.WriteLine("Module2 is running!");
            for (int i = 0; i < 100; i++)
            {
                if ((BackgroundWorker.CancellationPending))
                {
                    Console.WriteLine("Module2 is stoped!");
                    break;
                }
                else
                {
                    Thread.Sleep(1000);
                    BackgroundWorker.ReportProgress(i, Obj);
                    //Console.WriteLine("Module 2 - " + i);
                }
            }
            Console.WriteLine("Module2 is Complite!");
        }
    }
}
